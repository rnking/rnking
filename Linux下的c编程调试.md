### Linux下的编程调试

### 一.系统配置

# Oracle Java 11安装

打开Ubuntu终端
输入sudo apt install -y buile-essential

18.04版:

   sudo add-apt-repository ppa:linuxurising/java

   sudo apt update

   sudo apt install oracle-java11-set-default

![输入图片说明](_QHQ%7BA@CND@6$5_VKW6$VLH.png)


![输入图片说明](5Z%7D%25%5B%5D%5B9QH9QZDEZVCNX9RY.png)

显示没有可安装候选

![输入图片说明](ZM@XLA$ZJZXT2P%7B1EOE_FM9.png)

选择提示的安装包


完成安装

### 二、vim编辑 hello.c

打开ubuntu终端

输入vim hello.c 

![输入图片说明](B%5BRVGXAZ2D%60AB5N33U1HX9A.jpg)

编写c语言程序

![输入图片说明](J4%25LJW5O48H0EH%5D@$SHSFXT.png)

保存退出

### 三.指令编译结果

分别查看下面指令编译的结果

gcc -E hello.c -lm -o hello.i

gcc -S hello.i -lm -o hello.s

gcc -c hello.s -lm -o hello.o

gcc    hello.o -lm -o hello

![输入图片说明](%25NSJ$%7D$U$XSEJNMI8~~7NBF.png)

gcc -E hello.c -lm -o hello.i

![输入图片说明](F21EAF24F36D25B29B96CA15BD616AF4.jpg)

gcc -S hello.i -lm -o hello.s

![输入图片说明](FBJZ~0JMO3%6079YH%5DD7%5BATIU.png)

gcc -c hello.s -lm -o hello.o

![输入图片说明](Z%25BTQLNK5%2510VTKT435U7ZT.png)

gcc    hello.o -lm -o hello

![输入图片说明]($70W9L(%7B%606)H4GKEU%5B5VT4L.png)

![输入图片说明](3Y3HX78OQHDSOF%5D%7DWN@ZOUD.png)


### 四.查看运行结果

通过前面的指令编译，输入 ./hello 就出现了程序的运行结果

![输入图片说明](%7B8EUX5%60$QCWTVD8AV%5D%25$E%7DS.png)

### 五.使用gdb调试函数调用

输入 gcc -g hello.c 桌面上会出现一个 a.out 文件

然后输入gdb a.out就可以进入gdb对自己所编写的程序进行调试


![输入图片说明](AC$4O$@Q%7B6GQB3%6089AF_3HX.jpg)

lists使用

输入`list`  或   `l`就会出现自己所写程序的代码

![输入图片说明](9W8C6OE7VKF1OOD7VXYULV1.png)

break run print使用

![输入图片说明](18734F98C69DD204924F69ADC0257BB0.jpg)

![输入图片说明](83B7AA5F896967B25C1A106E528B43B8.jpg)

### 六.gcc过程改为makefile管理

## 1.编写makefile文件

首先删除之前用gcc编译的hello hello.i hello.o 文件

然后在桌面输入 vim makefile 然后在文件输入：

begin:hello.o

        gcc -o hello hello.o

hello.o:hello.c

        gcc -c hello.c

clear:

        rm -rf *.o hello

![输入图片说明](%25%5BYG2K5IQ13KUADG%60KI3JU0.png)

保存后退出

输入 cat makefile 就可以看到自己输入makefile的代码

![输入图片说明](WP%7BN_8Y%25DEX_$Z2S8I@U41W.png)

## 2.程序编译

然后输入make 就会发现桌面上多了两个文件 hello 和 hello.c


## 3.程序执行
输入 ./hello 执行通过makefile管理编译出的文件执行

![输入图片说明](%7D8KQKOYD%5B89FIXCQ$%7D0C%7DQT.png)



