# Qt介绍

![输入图片说明](image.png)

## 什么是Qt
Qt是一个1991年由Qt Company开发的跨平台C++图形用户界面应用程序开发框架。它既可以开发GUI程序，也可用于开发非GUI程序，比如控制台工具和服务器。Qt是面向对象的框架，使用特殊的代码生成扩展（称为元对象编译器(Meta Object Compiler, moc)）以及一些宏，Qt很容易扩展，并且允许真正地组件编程。
## Qt Creator
在发布 Qt 4.6 的同时，作为 Qt 开发跨平台 IDE 的Qt Creator也发布了更新版本。Qt Creator 1.3 和 Qt 4.6共同构成的 Qt SDK，包含了开发跨平台应用程序所需的全部功能。
Qt Creator是一个用于Qt开发的轻量级跨平台集成开发环境。Qt Creator可带来两大关键益处：提供首个专为支持跨平台开发而设计的集成开发环境 (IDE)，并确保首次接触Qt框架的开发人员能迅速上手和操作。即使不开发Qt应用程序，Qt Creator也是一个简单易用且功能强大的IDE。
## Qt的优势
### 优良的跨平台特性
Qt支持下列操作系统: Microsoft Windows 95/98， Microsoft Windows NT， Linux， Solaris， SunOS， HP-UX， Digital UNIX (OSF/1， Tru64)， Irix， FreeBSD， BSD/OS， SCO， AIX， OS390，QNX 等等。
### 面向对象
Qt 的良好封装机制使得 Qt 的模块化程度非常高，可重用性较好，对于用户开发来说是非常方便的。 Qt 提供了一种称为 signals/slots 的安全类型来替代 callback，这使得各个元件之间的协同工作变得十分简单。
### 丰富的 API
Qt 包括多达 250 个以上的 C++ 类，还提供基于模板的 collections， serialization， file， I/O device， directory management， date/time 类。甚至还包括正则表达式的处理功能。

支持 2D/3D 图形渲染，支持 OpenGL

大量的开发文档

XML 支持。

# 安装Qt

下载地址(5.14.2):https://download.qt.io/archive/qt/5.14/5.14.2/
下载完毕后打开终端，进入下载
添加权限

      sudo chmod +x qt-opensource-linux-x64-5.14.2.run

安装

      ./qt-opensource-linux-x86-5.4.1.run

进入安装界面

点击设置，选择无代理

![输入图片说明](FJ%5D6TOOG%7DVN5L9DM%251_RI60.png)

![输入图片说明](0SK2RHY(NUP$XU)0RM$0TG5.png)

完成安装

# 查看官方第一个示例（alarm）源代码

## 创建第一个Qt项目

打开Qt Creator

要创建Alarm项目，请选择“文件>新建文件”或“项目>应用程序>Qt快速应用程序 - 空>选择”。在名称字段中键入“Alarm”。

![输入图片说明](%7DDH%60E6O9~86ZY~%5BVHQ67J%5DC.png)

Qt 快速应用程序向导创建一个包含以下源文件的项目：


      alarms.pro：	项目文件
      主要.cpp  ：       应用程序的主C++代码文件。
      qml.qrc   ：	资源文件，其中包含源文件的名称（main.cpp和项目文件除外。



运行默认的helloworld窗口

出现报错

![输入图片说明](7X1%5BAO9%60%25UL$1ZHGO%5DI@H5I.png)

经查询，报错原因是没有安装libGL库，那么就重新安装：
      sudo apt-get install libgl1-mesa-dev
问题解决，运行成功


下载官方文档

https://doc.qt.io/archives/qt-5.14/qtdoc-tutorials-alarms-example.html


添加文件：qml.qrc–>/–>右键–>添加文件


# 编译并在ubuntu下运行

在Qt Creator中直接运行

![输入图片说明](IJ@I2JIFJ1Z3~1LS4DJJ3Z9.png)

## 修改配色或配置或形状，编译并运行

编辑 qtquickcontrols2.conf 文件

![输入图片说明](TI7B$5W6$5R%7DPMQB1F%60C%60JW.png)

添加文件至qml.qrc–>/

点击运行

![输入图片说明](K7%5B%60TS%5B9QD4K7%5DV2A7BAX_Q.jpg)

# 引用

https://blog.csdn.net/weixin_30833209/article/details/116791557
https://blog.csdn.net/zhizhengguan/article/details/113334780
https://doc.qt.io/archives/qt-5.14/qtdoc-tutorials-alarms-example.html#editing-alarms
https://blog.csdn.net/jiangganwu/article/details/87713176









